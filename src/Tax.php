<?php namespace FrancoCorrea\TaxManager;

/**
 * Main TaxManager Handler
 */
class Tax
{
  public $percentage;
  public $base_amount;

  private $last_action;
  private $last_action_result;

  function __construct(float $percentage)
  {
    $this->percentage = $percentage;
  }

  public function setBaseAmount(float $amount)
  {
    if (!$amount > 0)
    {
      throw new Exception("Amount must be greater than 0.");
    }

    return $this->base_amount = $amount;
  }

  public function addTax()
  {
    $base = $this->base_amount;
    $tax = $this->percentage;

    $this->last_action = "add";
    $this->last_action_result = $base + ($base * ($tax / 100));

    return $this->last_action_result;
  }

  public function removeTax()
  {
    $base = $this->base_amount;
    $tax = $this->percentage;

    $this->last_action = "remove";
    $this->last_action_result = ($base / (100 + $tax)) * 100;

    return $this->last_action_result;
  }

  public function lastTransactionDetails()
  {
    return [
      'base_amount' => $this->base_amount,
      'action' => $this->last_action,
      'result' => $this->last_action_result,
    ];
  }
}
