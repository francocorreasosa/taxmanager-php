<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitae051702296d2bdf862a7eb594c5a89d
{
    public static $prefixLengthsPsr4 = array (
        'F' => 
        array (
            'FrancoCorrea\\TaxManager\\' => 24,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'FrancoCorrea\\TaxManager\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitae051702296d2bdf862a7eb594c5a89d::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitae051702296d2bdf862a7eb594c5a89d::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
